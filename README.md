![](icon.png)

# Frisma
Unique colorscheme for overnight developers.


Based on [hybrid.vim](https://github.com/w0ng/vim-hybrid) and made with [terminal.sexy](https://terminal.sexy).

![](screenshots/neofetch.png)
![](screenshots/neovim.png)
